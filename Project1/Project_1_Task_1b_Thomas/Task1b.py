import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv('observation_features.csv')

""" Functions to compute RISK and VACCINE EFFICACY. """
def compute_risk(group):
    return sum(group[:, 1]) / len(group)

def compute_vaccine_efficacy(vaccinated_group, unvaccinated_group):
    return (compute_risk(unvaccinated_group) - compute_risk(vaccinated_group)) / compute_risk(unvaccinated_group)


""" Split the data according to vaccination status. """
# column no. containing the vaccination status
vac_status = [147, 148, 149]
vac0_group = df.values[np.where(np.all(df.values[:, vac_status] == 0, axis=1))]  # not vaccinated

# vac1_group = df.values[np.where(df.values[:, vac_status[0]] == 1)]  # vaccine no. 1
# vac2_group = df.values[np.where(df.values[:, vac_status[1]] == 1)]  # vaccine no. 2
# vac3_group = df.values[np.where(df.values[:, vac_status[2]] == 1)]  # vaccine no. 3

# Preparing Plots
fig, axs = plt.subplots(len(vac_status))
plt.setp(axs, ylim=(0, 50))

for i in range(len(vac_status)):
    # considering vaccine no. i
    vac_group = df.values[np.where(df.values[:, vac_status[i]] == 1)]
    print("VACCINE NO.", i)
    VE = []

    # The vaccine efficacy computed for ALL individuals from the vaccinated and unvaccinated groups
    overall_vaccine_efficacy = round(100 * compute_vaccine_efficacy(vac_group, vac0_group), 2)
    VE.append(overall_vaccine_efficacy)
    print("Overall Vaccine Efficacy of Vaccine 1:", overall_vaccine_efficacy, "%")

    """ Adjusting the analysis for Age (<55 vs >54) and Gender (f vs m). """
    # age adjusted groups
    index_age = 10
    age_adj_vac0 = vac0_group[np.where(vac0_group[:, index_age] < 55)]
    age_adj_vac = vac_group[np.where(vac_group[:, index_age] < 55)]
    age_adj_54 = round(100 * compute_vaccine_efficacy(age_adj_vac, age_adj_vac0), 2)
    VE.append(age_adj_54)
    print("Vaccine Efficacy for Vaccine", i+1, "for Age < 55:", age_adj_54, "%")
    # >= 55 years old
    age_adj_vac0 = vac0_group[np.where(vac0_group[:, index_age] > 54)]
    age_adj_vac = vac_group[np.where(vac_group[:, index_age] > 54)]
    age_adj_55 = round(100 * compute_vaccine_efficacy(age_adj_vac, age_adj_vac0), 2)
    VE.append(age_adj_55)
    print("Vaccine Efficacy for Vaccine", i+1, "for Age >= 55:", age_adj_55, "%")

    # gender adjusted groups
    index_sex = 11
    sex_adj_vac0 = vac0_group[np.where(vac0_group[:, index_sex] == 0)]
    sex_adj_vac = vac_group[np.where(vac_group[:, index_sex] == 0)]
    sex_adj_female = round(100 * compute_vaccine_efficacy(sex_adj_vac, sex_adj_vac0), 2)
    VE.append(sex_adj_female)
    print("Vaccine Efficacy for Vaccine", i+1, "for female patients:", sex_adj_female, "%")

    # male
    sex_adj_vac0 = vac0_group[np.where(vac0_group[:, index_sex] == 0)]
    sex_adj_vac = vac_group[np.where(vac_group[:, index_sex] == 0)]
    sex_adj_male = round(100 * compute_vaccine_efficacy(sex_adj_vac, sex_adj_vac0), 2)
    VE.append(sex_adj_male)
    print("Vaccine Efficacy for Vaccine", i+1, "for male patients:", sex_adj_male, "%")

    # Plotting
    x_labels = ["Overall", "<55", ">=55", "female", "male"]
    axs[i].bar(x_labels, VE)
    axs[i].set_ylabel("Vaccine Efficacy")

plt.show()












""" Appendix """
""" Get comparable data sets to determine the vaccine efficacy by (brute-force) matching with L2-distance. """


# We match individuals according to relevant features Age, Gender, Comorbidities (and obviously vaccination status)
matching_feature_indices = [10, 11, 142, 141, 143, 144, 145, 146]


def get_comparable_groups(vaccinated_group, unvaccinated_group, relevant_indices, delta=0.1):
    comp_group_vaccinated = []
    comp_group_unvaccinated = []
    for row in range(len(vaccinated_group)):
        min_index, min_dist = L2_nearest_vector(vaccinated_group[row], unvaccinated_group, relevant_indices)
        if min_dist < delta:
            comp_group_vaccinated.append(vaccinated_group[row])
            comp_group_unvaccinated.append(unvaccinated_group[min_index])
            # remove the row from the unvaccinated matching candidates
            unvaccinated_group = np.delete(unvaccinated_group, min_index, 0)
    return comp_group_vaccinated, comp_group_unvaccinated



def L2_nearest_vector(vector, group, relevant_indices):
    vector = vector[relevant_indices]
    group = group[:, relevant_indices]
    A = np.tile(vector, (len(group), 1))
    min_dist = np.min((np.sqrt((group - A) ** 2).sum(1)))
    min_index = np.argmin(((group - A) ** 2).sum(1))
    return min_index, min_dist

# # The vaccine efficacy computed for ALL individuals from the vaccinated and unvaccinated groups
# print("Vaccine Efficacy of Vaccine 1:", round(100*compute_vaccine_efficacy(vac1_group, vac0_group), 2), "%")
# print("Vaccine Efficacy of Vaccine 2:", round(100*compute_vaccine_efficacy(vac2_group, vac0_group), 2), "%")
# print("Vaccine Efficacy of Vaccine 3:", round(100*compute_vaccine_efficacy(vac3_group, vac0_group), 2), "%")
#
#
# """ Adjusting the analysis for Age (<55 vs >54), Gender, and Comorbidities. """
# # age adjusted groups
# index_age = 10
# age_adj_vac0 = vac0_group[np.where(vac0_group[:, index_age] < 55)]
# age_adj_vac1 = vac1_group[np.where(vac1_group[:, index_age] < 55)]
# age_adj_vac2 = vac2_group[np.where(vac2_group[:, index_age] < 55)]
# age_adj_vac3 = vac3_group[np.where(vac3_group[:, index_age] < 55)]
# print("Vaccine Efficacy for Vaccine 1 for Age < 55:", round(100*compute_vaccine_efficacy(age_adj_vac1, age_adj_vac0), 2), "%")
# print("Vaccine Efficacy for Vaccine 2 for Age < 55:", round(100*compute_vaccine_efficacy(age_adj_vac2, age_adj_vac0), 2), "%")
# print("Vaccine Efficacy for Vaccine 3 for Age < 55:", round(100*compute_vaccine_efficacy(age_adj_vac3, age_adj_vac0), 2), "%")
# print("Basis of Calculation N_0, N_1, N_2, N_3:", len(age_adj_vac0), len(age_adj_vac1), len(age_adj_vac2), len(age_adj_vac3))
#
# age_adj_vac0 = vac0_group[np.where(vac0_group[:, index_age] > 54)]
# age_adj_vac1 = vac1_group[np.where(vac1_group[:, index_age] > 54)]
# age_adj_vac2 = vac2_group[np.where(vac2_group[:, index_age] > 54)]
# age_adj_vac3 = vac3_group[np.where(vac3_group[:, index_age] > 54)]
# print("Vaccine Efficacy for Vaccine 1 for Age >= 55:", round(100*compute_vaccine_efficacy(age_adj_vac1, age_adj_vac0), 2), "%")
# print("Vaccine Efficacy for Vaccine 2 for Age >= 55:", round(100*compute_vaccine_efficacy(age_adj_vac2, age_adj_vac0), 2), "%")
# print("Vaccine Efficacy for Vaccine 3 for Age >= 55:", round(100*compute_vaccine_efficacy(age_adj_vac3, age_adj_vac0), 2), "%")
# print("Basis of Calculation N_0, N_1, N_2, N_3:", len(age_adj_vac0), len(age_adj_vac1), len(age_adj_vac2), len(age_adj_vac3))
#
# index_sex = 11
# sex_adj_vac0 = vac0_group[np.where(vac0_group[:, index_sex] == 0)]
# sex_adj_vac1 = vac1_group[np.where(vac0_group[:, index_sex] == 0)]
# sex_adj_vac2 = vac2_group[np.where(vac0_group[:, index_sex] == 0)]
# sex_adj_vac3 = vac3_group[np.where(vac0_group[:, index_sex] == 0)]